import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer.component';
import { HttpClientModule } from '@angular/common/http';
import { SongComponent } from './songs/songs.component';
import { AlbumComponent } from './album/album.component';
import { MainComponent } from './main/main.component';
import { RouterComponent } from './router/router.component';
import { AlbumListingComponent } from './album-listing/album-listing.component';
import { TemplateComponent } from './form/template/template.component';
import { ReactiveComponent } from './form/reactive/reactive.component';
import { HighlightDirective } from './directive/highlight.directive';
import { PipesExampleComponent } from './pipes-example/pipes-example.component';
import { ExponentialPipe } from './pipes/exponential.pipe';
import { FindlengthPipe } from './pipes/findlength.pipe';
import {PostCreateComponent} from './posts/post-create/post-create.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule,MatCardModule,MatButtonModule} from '@angular/material';



@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    SongComponent,
    AlbumComponent,
    MainComponent,
    RouterComponent,
    AlbumListingComponent,
    TemplateComponent,
    ReactiveComponent,
    HighlightDirective,
    PipesExampleComponent,
    ExponentialPipe,
    FindlengthPipe,
    PostCreateComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
