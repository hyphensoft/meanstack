import {Component} from '@angular/core';
@Component({
    selector : 'app-post-create',
    templateUrl:'./post-create.component.html',
    styleUrls : ['./post-create.component.css']
})
export class PostCreateComponent{

    newPost = 'No value';
    enteredValue = "Default Value";
    // onAddPost(inputValue: HTMLTextAreaElement){

    //     console.dir(inputValue.value);
    //     alert(inputValue.value);
    //     this.newPost = inputValue.value;
    //     //alert(this.newPost);
    // }

    onAddPost(){
        this.newPost = this.enteredValue;
    }

}