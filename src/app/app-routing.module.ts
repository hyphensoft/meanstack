import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumComponent } from './album/album.component';
import { MainComponent } from './main/main.component';
import { RouterComponent } from './router/router.component';
import { ReactiveComponent } from './form/reactive/reactive.component';
import { TemplateComponent } from './form/template/template.component';
import { PipesExampleComponent } from './pipes-example/pipes-example.component';


const routes: Routes = [
  {
    path:  '',
    component:  RouterComponent,
    children: [
    
      {
        path:  '',
        component:  MainComponent
      },
      {
        path:  'albums',
        component:  AlbumComponent
      },
      {
        path:  'template-form',
        component:  TemplateComponent
      },
      {
        path:  'reactive-form',
        component:  ReactiveComponent
      },
      {
        path: 'user',
        loadChildren:'./user/user.module#UserModule'
      },
      {
        path:  'pipes',
        component:  PipesExampleComponent

      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
