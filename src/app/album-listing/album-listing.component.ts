import { Component, OnInit ,Output, EventEmitter} from '@angular/core';
import { AudioService } from '../services/audio.service';

@Component({
  selector: 'app-album-listing',
  templateUrl: './album-listing.component.html',
  styleUrls: ['./album-listing.component.css']
})
export class AlbumListingComponent implements OnInit {

  album : Object;
  message: string = "Hola Mundo!"
  @Output() messageEvent = new EventEmitter<string>();

  constructor(private data: AudioService) { }

  ngOnInit() {
    this.data.getAlbumList().subscribe(data=>{
      this.album = data.results;
    })
  }


  sendMessage(data) {
    this.messageEvent.emit(data);
  }


}
