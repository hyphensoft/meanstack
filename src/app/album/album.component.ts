import { Component, OnInit} from '@angular/core';



@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {
 

  constructor() { }
  albumDetail: any;
  ngOnInit() {
    
  }
  receiveMessage($event) {
    this.albumDetail = $event
    window.scroll(0,0);
  }
  

}
