import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'findlength'
})
export class FindlengthPipe implements PipeTransform {

  transform(value: any, args?: any): any {
   return value.length;
  }

}
