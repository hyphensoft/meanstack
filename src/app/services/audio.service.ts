import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class AudioService {

  constructor(private http:HttpClient) { }

  getAudioList(){
    return this.http.get<any>('http://itunes.apple.com/lookup?id=16586443&entity=song');
  }

  getAlbumList(){
    return this.http.get<any>('http://itunes.apple.com/lookup?id=16586443&entity=album');
  }
}
