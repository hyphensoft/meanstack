import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {
 
  username_email: string = 'sample@email.com';
  constructor() { }

  ngOnInit() {
  }
  onFormSubmit(formData) {
    alert(formData);
    console.log(formData);
 }


}
