import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  angForm: FormGroup;
  submitted = false;
  isChecked = true;
  //color:string = '#007bff';

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.angForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(15)]],
      acceptTerms: [false, Validators.requiredTrue]
    });
  }

  get angFormData() {
    return this.angForm.controls;
  }

  onReactiveFormSubmit(data, username, password) {
    console.log(data, username, password);
  }

}
