import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-songs',
  templateUrl: './songs.component.html',
  styleUrls: ['./songs.component.css']
})
export class SongComponent implements OnInit {
  //audio : Object;
  @Input() songsOnchild: any;

  constructor() { }

  ngOnInit() {
    console.log(this.songsOnchild);
  }

}
