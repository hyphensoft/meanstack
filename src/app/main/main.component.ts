import { Component, OnInit } from '@angular/core';
import {AudioService} from '../services/audio.service';

@Component({
  selector: 'app-main',
  template: '<app-songs [songsOnchild]="songsFromParent"></app-songs>',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  songsFromParent = [];

  constructor(private data: AudioService) { }

  ngOnInit() {
    this.data.getAudioList().subscribe(data =>{
      this.songsFromParent = data.results;
    })
    
  }

}
